import asyncio
import os
import random
import sys
import traceback
from io import BytesIO, StringIO
from pathlib import Path

from telethon import events, Button

from ..func.funcs import bash
from .. import uclient, client, adb, db, mdb
from ..decorators import bot_cmd


@bot_cmd(cmd="bash", admins_only=True)
async def _bash(e):
    try:
        cmd = e.text.split(" ", maxsplit=1)[1]
    except IndexError:
        return await e.reply("No CMD given.")
    xx = await e.reply("<code>Processing...</code>", parse_mode="html")
    stdout, stderr = await bash(cmd)
    OUT = f"⭐ <b>BASH\n\n• COMMAND:</b>\n<code>{cmd}</code> \n\n"
    err, out = "", ""
    if stderr:
        err = f"<b>• ERROR:</b> \n<code>{stderr}</code>\n\n"
    if stdout:
        out = f"<b>• OUTPUT:</b>\n<code>{stdout}</code>"
    if not stderr and not stdout:
        out = "<b>• OUTPUT:</b>\n<code>Success</code>"
    OUT += err + out
    if len(OUT) > 4096:
        txt = err + out
        with BytesIO(str.encode(txt)) as out_file:
            out_file.name = "_bash.txt"
            await bot.send_file(
                e.chat_id,
                out_file,
                caption=f"<code>{cmd[:1023]}</code>",
                reply_to=e.reply_to_msg_id,
                parse_mode="html",
            )
            await xx.delete()
    else:
        await xx.edit(OUT, parse_mode="html")


p = print


@bot_cmd(cmd="eval", admins_only=True)
async def _eval(e):
    try:
        cmd = e.text.split(maxsplit=1)[1]
    except IndexError:
        return await e.reply("Give Some CMD")

    xx = await e.reply("<code>Processing...</code>", parse_mode="html")
    old_stderr = sys.stderr
    old_stdout = sys.stdout
    redirected_output = sys.stdout = StringIO()
    redirected_error = sys.stderr = StringIO()
    stdout, stderr, exc = None, None, None
    try:
        value = await aexec(cmd, e)
    except Exception:
        value = None
        exc = traceback.format_exc()
    stdout = redirected_output.getvalue()
    stderr = redirected_error.getvalue()
    sys.stdout = old_stdout
    sys.stderr = old_stderr
    evaluation = exc or stderr or stdout
    final_output = (
        "⭐ __►__ <b>EVAL</b> \n<code>{}</code> \n\n __►__ <b>OUTPUT:</b> \n<code>{}</code> \n".format(
            cmd,
            evaluation,
        )
    )
    if len(final_output) > 4096:
        final_output = evaluation
        with BytesIO(str.encode(final_output)) as out_file:
            out_file.name = "_eval.txt"
            await e.client.send_file(
                e.chat_id,
                out_file,
                caption=f"<code>{cmd[:1020]}</code>",
                reply_to=e.reply_to_msg_id,
                parse_mode="html",
            )
        return await xx.delete()
    await xx.edit(final_output, parse_mode="html")


async def aexec(code, event):
    exec(
        (
            "async def __aexec(e, client): "
            + "\n p = print"
            + "\n message = event = e"
            + "\n reply = await event.get_reply_message()"
            + "\n chat = event.chat_id"
        )
        + "".join(f"\n {l}" for l in code.split("\n"))
    )

    return await locals()["__aexec"](event, event.client)
